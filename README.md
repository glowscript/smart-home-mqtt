Langkah :
1.	Merangkai alat yang dibutuhkan

![S__18432022](/uploads/fe192010523ed65ae8a124fdc64bed59/S__18432022.jpg)

2.	Buat program untuk sensor menggunakkan bahasa python
3.	Buat koneksi broker dengan topic (sensor lampu, sensor suhu, sensor kelembaban udara, sensor api, dan sensor pendeteksi hujan) tertentu

4.	Mempublish data dengan topik tertentu untuk disimpan ke broker 

5.	Membuat flow dashbord aplikasi menggunakan Node-Red

![messageImage_1557588788697](/uploads/28f60a35ff36084ea77e433e782def10/messageImage_1557588788697.jpg)

- posisi sedang tidak ada hujan dan tidak ada api

![not_detected](/uploads/48c30c1f1aed08423674160eee160011/not_detected.jpg)

- Posisi sedang ada api dan ada hujan 

![detected](/uploads/d6b72d8ac05bf3f08b778c89c29f746e/detected.jpg)

6.	Membuat MQTT Connection pada Node-Red dengan broker iot.eclipse.org:1883
7.	Men subscribe topik yang telah dipublish dari Raspberry Pi untuk ditampilkan pada dashboard

## berikut demo aplikasi
![sister](/uploads/f4a6615e2bf68b9ba99c2208cfef6354/sister.mp4)
