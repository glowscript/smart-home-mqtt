from gpiozero import InputDevice
from mqtt_client import *

no_rain = InputDevice(23)
channel = 21

GPIO.setmoode(GPIO.BCM)
GPIO.setup(channel, GPIO.IN)


temperature_topic = "house/temperature"
humidity_topic = "house/humidity"
rain_topic = "house/rain"
flame_topic = "house/flame"


def callback(channel):
    detectFlame = "Flame Detected"
    client.publish(flame_topic, detectFlame)
    print(detectFlame)

GPIO.add_event_detect(channel, GPIO.BOTH, bouncetime=300)
GPIO.add_event_callback(channel, callback)

def nodetect():
    detect = "Flame Not Detected"
    client.publish(flame_topic, detect)
    print(detect)
    detectRain = "Not Raining"
    client.publish(rain_topic, detectRain)
    print(detectRain)


def temp_humidity():
    humidity, temperature = dht.read_retry(dht.DHT11, 17)
    print('Sending... Temperature: {}   Humidity: {}'.format(temperature, humidity))
    client.publish(temperature_topic, temperature)
    client.publish(humidity_topic, humidity)
    
while True:
    temp_humidity()
    nodetect()
    if not no_rain.is_active:
        rain = "It's Raining"
        client.publish(rain_topic, rain)

    
    