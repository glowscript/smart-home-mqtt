import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt
import Adafruit_DHT as dht
import time
from sensor import *

client = mqtt.Client()
print("connecting to broker")
client.connect("test.mosquitto.org", port=1883)    

def on_message(client, userdata, msg):
    print("message recieved ", str(msg.payload.decode("utf-8")))
    
    GPIO.setmode(GPIO.BCM)
    
    if msg.payload.decode("utf-8") == "on":
        print("Turn On")
        GPIO.setup(18, GPIO.OUT)

    if msg.payload.decode("utf-8") == "off":
        print("Turn Off")
        GPIO.cleanup()

print("subscribing to topic", "house/bulbs/bulb1")
client.subscribe("house/bulbs/bulb1")
client.on_message = on_message

client.loop_start()


